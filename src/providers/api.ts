import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { Events } from "ionic-angular";
import { baseUrlApi } from "../app/app.module";
import { ContestResultType } from "../model/ContestResultType";

@Injectable()
export class ApiProvider {
    articles = [];
    contest = {};

    constructor(private http: HttpClient, private storage: Storage, public events: Events) {

    }

    public async init() {
        this.articles = await this.getArticlesFromApi();
    }

    getArticlesFromStorage() {
        return this.storage.get("articles").then((data) => {
            if (data != null) {
                this.articles = data;
                console.log("articles loaded from storage");
                return data;
            }
        });
    }

    async getArticlesFromApi() {
        try {
            return await this.http.get(baseUrlApi + "articles")
                .map(async (data: any) => {
                    this.articles = [];
                    data.forEach(element => {
                        this.articles[element.key] = element.text;
                    });
                    console.log("articles loaded from API");
                    this.storage.set("articles", this.articles);
                    return this.articles;
                }).toPromise();
        } catch (error) {
            console.log("maybe connection error:" + error.status);
            console.log("Oooops! " + JSON.stringify(error));
            return undefined;
        }
    }

    getArticles() {
        return this.articles;
    }

    getContestFromStorage() {
        return this.storage.get("contest").then((data) => {
            if (data != null) {
                this.contest = data;
                console.log("contest loaded from storage");
                return data;
            }
        });
    }

    async getContestFromApi(manageBadges: boolean = true) {
        const lastContestId = await this.storage.get("lastContestId");
        console.log("last contest id:" + lastContestId);

        try {
            return await this.http.get(baseUrlApi + "actualContest")
                .map(async (data: any) => {
                    this.contest = data;
                    console.log("contest :" + JSON.stringify(data));
                    console.log("contest id:" + data.id);


                    if (data.id === lastContestId)  {
                        console.log("contest stejna");
                    } else {
                        console.log("contest jina");
                        if (manageBadges) {
                            this.events.publish("tabs-page:badge-update", "contest", 1);
                        }
                    }

                    console.log("contests loaded from API:" + this.contest);
                    this.storage.set("contest", this.contest);
                    return this.contest;
                }).toPromise();
        } catch (error) {
            if (error && error.status === 404 && manageBadges) {
                this.events.publish("tabs-page:badge-update", "contest", 0);
            }
            console.log("maybe connection error");
            console.log("Oooops! " + JSON.stringify(error));
            return undefined;
        }
    }

    getContest() {
        return this.contest;
    }

    async sendContestResult(contestResult: ContestResultType) {
        try {
            let data: string = JSON.stringify(contestResult);
            let response: any;
            let headers: HttpHeaders = new HttpHeaders({
                "Content-Type": "application/json"
            });
            await this.http.post(baseUrlApi + "actualContest/results", data, {headers: headers}).toPromise();
            console.log("odeslano");
            return {
                status: "ok",
                data: response
            };
        } catch (error) {
            if (error.status === 409) {
                // uz bylo hlasovano
                return {
                    status: 409
                };
            }
            console.log("maybe connection error:" + error.status);
            console.log("Oooops! " + JSON.stringify(error));
            return undefined;
        }
    }
}
