import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { MyApp } from "./app.component";

import { TabsPage } from "../pages/tabs/tabs";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { ContestPage } from "../pages/contest/contest";
import { InfoPage } from "../pages/info/info";
import { ConfiguratorPage } from "../pages/configurator/configurator";
import { ProductsPage } from "../pages/products/products";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { ComponentsModule } from "../components/components.module";
import { IonicStorageModule } from "@ionic/storage";
import { HttpClientModule } from "@angular/common/http";
import { FCM } from "@ionic-native/fcm";
import { GoogleAnalytics } from "@ionic-native/google-analytics";
import { File } from "@ionic-native/file";

import { ApiProvider } from "../providers/api";
import { NotificationsProvider } from "../providers/notifications";
import { AnalyticsProvider } from "../providers/analytics";
import { Keyboard } from "@ionic-native/keyboard";

export const baseUrl: string = "https://appadmin.livesweaters.eu";
export const baseUrlApi: string = baseUrl + "/api/";
export const analyticsKey: string = "UA-138232274-1";

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    ContestPage,
    ConfiguratorPage,
    ProductsPage,
    InfoPage
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: "Zpět"
    }),
    IonicStorageModule.forRoot(),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ContestPage,
    ProductsPage,
    ConfiguratorPage,
    InfoPage,
    TabsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FCM,
    GoogleAnalytics,
    ApiProvider,
    File,
    Keyboard,
    NotificationsProvider,
    AnalyticsProvider
  ]
})
export class AppModule {
 public static iniframeReuse: boolean = false;
}
