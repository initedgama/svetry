import { Component, ViewChild} from "@angular/core";
import { Platform, Nav, Events } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { TabsPage } from "../pages/tabs/tabs";
import { ApiProvider } from "../providers/api";
import { NotificationsProvider } from "../providers/notifications";
import { AnalyticsProvider } from "../providers/analytics";
import { Keyboard } from "@ionic-native/keyboard";

declare var iniframe: any;

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = TabsPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, apiProv: ApiProvider, notificationsProvider: NotificationsProvider, analytics: AnalyticsProvider, public keyboard: Keyboard, private events: Events) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      apiProv.init();
      analytics.init();
      notificationsProvider.notificationSetup(this.nav);
      apiProv.getContestFromApi();
      if (typeof iniframe !== "undefined") {
        iniframe.create();
        this.events.publish("iniframeCreated");
      }

      this.keyboard.onKeyboardShow().subscribe(() => {
        document.body.classList.add("keyboard-is-open");
      });

      this.keyboard.onKeyboardHide().subscribe(() => {
        document.body.classList.remove("keyboard-is-open");
      });
    });
  }
}
