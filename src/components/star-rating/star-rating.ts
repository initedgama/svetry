import { Component, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "star-rating",
  templateUrl: "star-rating.html"
})
export class StarRatingComponent {

  rating: number = 3;
  @Output() ratingChanged = new EventEmitter<number>();

  constructor() {
  }

  changeRating(event) {
    this.rating = event.target.id ? event.target.id : event.target.parentElement.id;
    this.ratingChanged.emit(this.rating);
  }

}
