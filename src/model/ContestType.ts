import { QuestionType } from "./QuestionType";

export interface ContestType {
  id: number;
  created: any;
  finishOn: any;
  status: string;
  contestType: QuestionType;
  question: string;
  image: string;
  answers: string[];
}
