export enum QuestionType {
  VOTING = "voting",
  RATING = "rating",
  TEXT = "text"
}
