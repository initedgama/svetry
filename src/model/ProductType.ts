export enum ProductType {
  BUSINESS = 0,
  DESIGN = 1,
  GOLF = 2,
  ACCESSORIES = 3
}
