export interface ContestResultType {
  name: string;
  email: string;
  answers: string[];
}
