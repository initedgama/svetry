import { Component, ViewChild, ElementRef } from "@angular/core";
import { Events, NavController, NavParams, Platform } from "ionic-angular";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { AppModule } from "../../app/app.module";
import { File } from "@ionic-native/file";

declare var iniframe: any;

@Component({
  selector: "page-products",
  templateUrl: "products.html",
})
export class ProductsPage {

  @ViewChild("iframe")
  iframeComponent: ElementRef;

  public url: SafeUrl;
  public title: string = "PRODUKTY";

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, private sanitizer: DomSanitizer, private platform: Platform, private file: File) {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl("about:blank");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ProductsPage");
    this.events.publish("tabs-page:badge-update", "configurator", 0);
    // this.url = this.sanitizer.bypassSecurityTrustResourceUrl("http://www.livesweaters.eu/cs/info/svetr-na-miru?aplikace=yes");
    this.events.subscribe("iniframeCreated", () => {
      this.ionViewWillEnter();
      this.ionViewDidEnter();
    });
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter ProductsPage");
    AppModule.iniframeReuse = true;

    if (typeof iniframe !== "undefined") {
      const fileUrl = this.file.applicationDirectory + "www/assets/html/start.html";
      console.log("file url: " + fileUrl);
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl("./assets/html/start.html");
      iniframe.navigate(fileUrl);
      iniframe.show();
    } else {
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl("./assets/html/start.html");
    }
  }

  ionViewDidEnter() {
    console.log("ionViewDidEnter ProductsPage");

    if (typeof iniframe !== "undefined") {
      const statutBarHeight = this.platform.height() - window.innerHeight;
      const tabBarHeight = this.platform.is("ios") ? 49.5 : 56;
      const safeAreaBottom = document.getElementById("bottom-safe-area-helper").getBoundingClientRect().height;
      const iniFrameHeight = this.platform.height() - statutBarHeight - tabBarHeight - safeAreaBottom;
      iniframe.resize(0, statutBarHeight, this.platform.width(), iniFrameHeight);
    }



    // this.url = this.sanitizer.bypassSecurityTrustResourceUrl("http://www.livesweaters.eu/cs/info/svetr-na-miru?aplikace=yes&t=" + (new Date).getTime());
    // this.url = this.sanitizer.bypassSecurityTrustResourceUrl("http://p5580.dev.inited.cz/cs/info/1.html?aplikace=yes&t=" + (new Date).getTime());
    // this.splashScreen.show(); // TEST VOLANI NATIVE CODE
    // const browser = this.inAppBrowser.create("http://p5580.dev.inited.cz/cs/info/1.html?aplikace=yes&t=" + (new Date).getTime());
  }

  ionViewWillLeave() {
    console.log("ionViewWillLeave ProductsPage");
    AppModule.iniframeReuse = false;
  }

  ionViewDidLeave() {
    console.log("ionViewDidLeave ProductsPage");
    if (!AppModule.iniframeReuse && (typeof iniframe !== "undefined")) {
      iniframe.hide();
    }
  }

  // funkce je volana pokud je aktivni tento view a uzivatel znovu klikne na aktivni tab button
  ionSelected() {
    this.ionViewWillEnter();
  }
}
