import { Component, ViewChild } from "@angular/core";
import { AlertController, App, Events, Loading, LoadingController, NavController, NavParams, Slides } from "ionic-angular";
import { QuestionType } from "../../model/QuestionType";
import { ContestType } from "../../model/ContestType";
import { ContestResultType } from "../../model/ContestResultType";
import { ApiProvider } from "../../providers/api";
import { Storage } from "@ionic/storage";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";

@Component({
  selector: "page-contest",
  templateUrl: "contest.html",
})
export class ContestPage {
  @ViewChild("slides") slides: Slides;

  answerVoting: string;
  showForm: boolean = false;
  userName: string = "";
  userEmail: string = "";
  answerText: string = "";
  answerRating: number[];
  contest: ContestType;
  loading: Loading;
  actualSlide: number = 1;
  maxSlide: number = 1;
  policy: boolean = false;
  isLoading: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public events: Events, public api: ApiProvider, private storage: Storage, private loadingCtrl: LoadingController, private app: App, private browser: InAppBrowser) {
      storage.get("userName").then((value) => this.userName = value);
      storage.get("userEmail").then((value) => this.userEmail = value);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ContestPage");
    this.events.publish("tabs-page:badge-update", "contest", 0);
  }

  ionViewDidEnter() {
    this.isLoading = true;
    this.actualSlide = 1;
    if (this.slides) {
      this.slides.slideTo(0);
    }
    this.loading = this.loadingCtrl.create({
        content: "Načítám...",
        enableBackdropDismiss: true,
        showBackdrop: true
      }
    );
    this.loading.present();
    this.api.getContestFromApi(false).then(this.prepareContest.bind(this));
  }

  ionViewDidLeave() {
    this.answerVoting = "";
    this.showForm = false;
  }

  prepareContest(contest) {
    console.log("mam contest", contest);
    if (contest && contest.id) {
      this.storage.set("lastContestId", contest.id);
    }
    this.showForm = false;
    this.answerText = "";
    this.answerVoting = "";
    this.policy = false;
    this.contest = contest;
    this.loading.dismissAll();
    this.isLoading = false;
    if (this.contest && this.contest.contestType === QuestionType.RATING) {
      this.maxSlide = this.contest.answers.length + 1; // + formular
      this.answerRating = [];
      this.contest.answers.forEach(() => { this.answerRating.push(3); }); // defaultne jsou 3 hvezdicky
    }
  }

  slideChanged (event) {
    this.actualSlide = event.realIndex + 1;
  }

  // direction = -1 > left
  // direction = 1 > right
  slidesSwipe(direction) {
    if (this.actualSlide === 1 && direction < 0) {
      return;
    }

    if (this.actualSlide === this.maxSlide && direction > 0) {
      return;
    }

    this.slides.slideTo(this.slides.realIndex + direction);
  }

  openPolicy(event) {
    event.stopPropagation();
    this.browser.create("http://www.livesweaters.eu/cs/info/podminky-ochrany-osobnich-udaju", "_system");
  }

  onRatingEmitted(imageIndex: number, rating: number) {
    console.log("rating", imageIndex, rating);
    this.answerRating[imageIndex] = rating;
    this.answerVoting = "ok"; // povolit odesilaci tlacitko
  }

  selectAnswer(answer: any): void {
    this.answerVoting = answer;
  }

  nextPage(): void {
    this.showForm = true;
  }

  async sendResult() {
    this.storage.set("userName", this.userName);
    this.storage.set("userEmail", this.userEmail);
    const answers: string[] = [];

    switch (this.contest.contestType) {
    case QuestionType.VOTING:
      answers.push(this.answerVoting);
      break;
    case QuestionType.RATING:
      this.answerRating.forEach((index) => { answers.push("" + index); });
      break;
    case QuestionType.TEXT:
      answers.push(this.answerText);
      break;
    }

    const contestResult: ContestResultType = {
      name: this.userName,
      email: this.userEmail,
      answers: answers
    };
    console.log("sending answer", contestResult);
    const answerStatus = await this.api.sendContestResult(contestResult);
    let title;
    let msg;
    let is2x = false;
    if (answerStatus && (answerStatus.status === 409)) {
        title = "Nelze odeslat";
        msg = "Nelze hlasovat 2x";
        is2x = true;
    } else {
        title = "Odpověď byla odeslána.";
        msg = "Děkujeme za účast v soutěži.";
    }
    this.alertCtrl.create({
      title: title,
      message: msg,
      buttons: [
        {
          text: "OK",
          handler: () => {
            if (!is2x) {
              this.app.getRootNav().getActiveChildNav().select(0);
            }
          }
        }
      ]
    }).present();
  }

  // todo promyslet jak se budu vracet v jednotlivych castech odpovedi
  public back(): void {
    this.showForm = false;
  }
}
