import { Component } from "@angular/core";
import { Events, NavController, NavParams } from "ionic-angular";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { ApiProvider } from "../../providers/api";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "page-info",
  templateUrl: "info.html",
})
export class InfoPage {

  private contacts: any = {
    "link-facebook": "https://www.facebook.com/LiveSweaters",
    "link-instagram": "https://www.instagram.com/livesweaters/",
    "link-web": "http://beta.livesweaters_eshop.soccer.wpsh.cz/",
    "link-email": "mailto:michal@livesweaters.com"
  };
  text;

  constructor(public navCtrl: NavController, public navParams: NavParams, private inAppBrowser: InAppBrowser, public events: Events, private apiProv: ApiProvider, private sanitizer: DomSanitizer) {
    let articles = this.apiProv.getArticles();
    if (articles) {
      this.text = this.sanitizer.bypassSecurityTrustHtml(articles["o-nas"]);
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad InfoPage");
    this.events.publish("tabs-page:badge-update", "info", 0);
  }

  public openBrowser(link): void {
    this.inAppBrowser.create(this.contacts[link], "_system");
  }

  textOnclick(ev) {
    if (ev.target && ev.target.href) {
      console.log("Click on <a> in text, href:" + ev.target.href);
      this.inAppBrowser.create(ev.target.href, "_system");
      ev.preventDefault();
    }
  }
}
