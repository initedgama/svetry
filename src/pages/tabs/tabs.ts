import { Component, ViewChild } from "@angular/core";

import { InfoPage } from "../info/info";
import { ProductsPage } from "../products/products";
import { ContestPage } from "../contest/contest";
import { Events, NavParams, Tabs } from "ionic-angular";
import { ConfiguratorPage } from "../configurator/configurator";

@Component({
  templateUrl: "tabs.html"
})
export class TabsPage {
  // @ViewChild(SwipeTabDirective) swipeTabDirective: SwipeTabDirective;
  @ViewChild("myTabs") tabRef: Tabs;

  public tab1Root = ProductsPage;
  public tab2Root = ConfiguratorPage;
  public tab3Root = ContestPage;
  public tab4Root = InfoPage;

  public mySelectedIndex: number;

  public tabBadgeCount: any = {"catalog": 0, "configurator": 0, "contest": 0, "info": 0};

  constructor(public navParams: NavParams, public events: Events) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
    this.subscribeToBadgeCountChange();
  }

  public subscribeToBadgeCountChange(): void {
    this.events.subscribe("tabs-page:badge-update", (tab, count) => {
      console.log("refreshBadgeCount", tab, count);
      this.tabBadgeCount[tab] = count;
    });
  }


  // tohle dělá jen přepnutí
  public transition($event): void {
    console.log("transition " + $event.index);
    $event.setRoot($event.root);
  }

  // tohle dělá přepnutí i swipe
  public onTabChange(index: number): void {
    console.log("onTabChange " + index);
    this.tabRef.select(index);

    // todo pro testovani

  }
}
