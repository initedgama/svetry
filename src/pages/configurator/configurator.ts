import { Component, ViewChild, ElementRef } from "@angular/core";
import { Events, NavController, NavParams, Platform, Navbar } from "ionic-angular";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { AppModule } from "../../app/app.module";

declare var iniframe: any;

@Component({
  selector: "page-configurator",
  templateUrl: "configurator.html",
})
export class ConfiguratorPage {

  @ViewChild("iframe")
  iframeComponent: ElementRef;

  public url: SafeUrl;
  public title: string = "KONFIGURÁTOR";

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, private sanitizer: DomSanitizer, private platform: Platform) {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl("about:blank");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ConfiguratorPage");
    this.events.publish("tabs-page:badge-update", "configurator", 0);
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl("http://www.livesweaters.eu/cs/info/svetr-na-miru?aplikace=yes");
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter ConfiguratorPage");
    AppModule.iniframeReuse = true;

    if (typeof iniframe !== "undefined") {
      iniframe.navigate("http://www.livesweaters.eu/cs/info/svetr-na-miru?aplikace=yes&t=" + (new Date).getTime());
      iniframe.show();
    }
  }

  ionViewDidEnter() {
    console.log("ionViewDidEnter ConfiguratorPage");

    if (typeof iniframe !== "undefined") {
      const statutBarHeight = this.platform.height() - window.innerHeight;
      const tabBarHeight = this.platform.is("ios") ? 49.5 : 56;
      const safeAreaBottom = document.getElementById("bottom-safe-area-helper").getBoundingClientRect().height;
      const iniFrameHeight = this.platform.height() - statutBarHeight - tabBarHeight - safeAreaBottom;
      iniframe.resize(0, statutBarHeight, this.platform.width(), iniFrameHeight);
    }

    // this.url = this.sanitizer.bypassSecurityTrustResourceUrl("http://www.livesweaters.eu/cs/info/svetr-na-miru?aplikace=yes&t=" + (new Date).getTime());
    // this.url = this.sanitizer.bypassSecurityTrustResourceUrl("http://p5580.dev.inited.cz/cs/info/1.html?aplikace=yes&t=" + (new Date).getTime());
    // this.splashScreen.show(); // TEST VOLANI NATIVE CODE
    // const browser = this.inAppBrowser.create("http://p5580.dev.inited.cz/cs/info/1.html?aplikace=yes&t=" + (new Date).getTime());
  }

  ionViewWillLeave() {
    console.log("ionViewWillLeave ConfiguratorPage");
    AppModule.iniframeReuse = false;

    // Unregister the custom back button action for this page
    //    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLeave() {
    console.log("ionViewDidLeave ConfiguratorPage");
    if (!AppModule.iniframeReuse && (typeof iniframe !== "undefined")) {
      iniframe.hide();
    }
  }

  // funkce je volana pokud je aktivni tento view a uzivatel znovu klikne na aktivni tab button
  ionSelected() {
    this.ionViewWillEnter();
  }
}
