
module.exports = {
    create: function () {
        cordova.exec(null, null, "INIFrame", "create", []);
    },
    show: function () {
        cordova.exec(null, null, "INIFrame", "show", []);
    },
    hide: function () {
        cordova.exec(null, null, "INIFrame", "hide", []);
    },
    resize: function (x, y, w, h) {
        cordova.exec(null, null, "INIFrame", "resize", [x,y,w,h]);
    },
    navigate: function (url) {
        cordova.exec(null, null, "INIFrame", "navigate", [url]);
    },
    back: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "INIFrame", "back", []);
    },
    destroy: function () {
        cordova.exec(null, null, "INIFrame", "destroy", []);
    }
};

