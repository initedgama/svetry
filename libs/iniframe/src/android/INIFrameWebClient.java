package cz.inited.plugin.iniframe;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class INIFrameWebClient extends WebViewClient {

    private boolean doClearHistory = false;

    public void clearHistory() {
        doClearHistory = true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        if (doClearHistory) {
            view.clearHistory();
            doClearHistory = false;
        }
    }

}
