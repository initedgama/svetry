package cz.inited.plugin.iniframe;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import android.app.Dialog;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class INIFrame extends CordovaPlugin {


    private WebView wv;
    private INIFrameWebClient webClient;
    static final String TAG = "INIFrame";

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {


        System.out.println("execute: " + action);

        if (action.equals("greet")) {

            String name = data.getString(0);
            String message = "Hello, " + name;
            callbackContext.success(message);

            return true;

        } if (action.equals("create")) {

            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    createWebView();
                }
            });

            return true;

        } if (action.equals("resize")) {

            final int x = data.getInt(0);
            final int y = data.getInt(1);
            final int w = data.getInt(2);
            final int h = data.getInt(3);

            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    resizeWebView(x, y, w, h);
                }
            });

            return true;

        } if (action.equals("navigate")) {

            final String url = data.getString(0);

            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    navigateWebView(url);
                }
            });

            return true;

        } if (action.equals("show")) {

            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showWebView();
                }
            });

            return true;

        } if (action.equals("hide")) {

            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideWebView();
                }
            });

            return true;

        } if (action.equals("back")) {

            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean canGoBack = backWebView();
                    callbackContext.success("" + canGoBack);
                }
            });

            return true;

        } else {
            LOG.e(TAG, "Unsupported iniframe action:" + action);
            return false;

        }
    }


    private void createWebView() {
        wv = new INIFrameWebView(cordova.getActivity());

        // enable javascript
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // enable open pages in me
        webClient = new INIFrameWebClient();
        wv.setWebViewClient(webClient);


        DisplayMetrics dm = new DisplayMetrics();
        this.cordova.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;


        FrameLayout.LayoutParams lp2 = new FrameLayout.LayoutParams(100, 100);

        cordova.getActivity().addContentView(wv, lp2);

    }

    private void resizeWebView(int x, int y, int w, int h) {

        if (wv == null) {
            return;
        }

        DisplayMetrics dm = new DisplayMetrics();
        this.cordova.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        w = Math.round(w * dm.density);
        h = Math.round(h * dm.density);

        FrameLayout.LayoutParams lp2 = new FrameLayout.LayoutParams(w, h);
        lp2.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp2.height = WindowManager.LayoutParams.MATCH_PARENT;

        lp2.width = w;
        lp2.height = h;
        lp2.topMargin = y;
        lp2.leftMargin = x;

        wv.setLayoutParams(lp2);
    }

    private void navigateWebView(String url) {
        if (wv == null) {
            return;
        }
        webClient.clearHistory();
        wv.loadUrl(url);
    }

    private void showWebView() {
        if (wv == null) {
            return;
        }
        wv.setVisibility(View.VISIBLE);
    }

    private void hideWebView() {
        if (wv == null) {
            return;
        }
        wv.setVisibility(View.GONE);
    }

    private boolean backWebView() {
        if (wv == null) {
            return false;
        }

        if (!wv.canGoBack()) {
            return false;
        }
        wv.goBack();
        return true;
    }

}

