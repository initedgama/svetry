package cz.inited.plugin.iniframe;

import android.app.Activity;
import android.view.KeyEvent;
import android.webkit.WebView;

public class INIFrameWebView extends WebView {

    public INIFrameWebView(Activity activity) {
        super(activity);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (this.canGoBack()) {
                this.goBack();
                return true;
            }
        }
        return false;
    }

}
