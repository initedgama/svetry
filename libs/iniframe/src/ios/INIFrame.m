#import "INIFrame.h"
#import <WebKit/WebKit.h>

@implementation INIFrame
{
    WKWebView *webView;
}


- (void) create:(CDVInvokedUrlCommand*)command
{
    [self createWebView];
}

- (void) show:(CDVInvokedUrlCommand*)command
{
    [self showWebView];
}

- (void) hide:(CDVInvokedUrlCommand*)command
{
    [self hideWebView];
}

- (void) resize:(CDVInvokedUrlCommand*)command
{
    CGFloat x = (CGFloat)[command.arguments[0] floatValue];
    CGFloat y = (CGFloat)[command.arguments[1] floatValue];
    CGFloat width = (CGFloat)[command.arguments[2] floatValue];
    CGFloat height = (CGFloat)[command.arguments[3] floatValue];
    [self resizeWebViewX:x y:y width:width height:height];
}

- (void) navigate:(CDVInvokedUrlCommand*)command
{
    NSString* url = [[command arguments] objectAtIndex:0];
    [self navigateWebViewToURLWithString:url];
}

- (void) destroy:(CDVInvokedUrlCommand*)command
{
    [self hideWebView];
    [self destroyWebView];
}



- (void) createWebView {
    if (!webView) {
        webView = [[WKWebView alloc] init];
        webView.allowsBackForwardNavigationGestures = TRUE;
    }
}

- (void) showWebView {
    if (!webView.superview) {
        [self.viewController.view addSubview:webView];
    }
}

- (void) hideWebView {
    if (webView.superview) {
        [webView removeFromSuperview];
    }
}

- (void) resizeWebViewX:(CGFloat)x y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height {
    if (webView.superview) {
        webView.frame = CGRectMake(x, y, width, height);
        [self.viewController.view layoutIfNeeded];
    }
}

- (void) navigateWebViewToURLWithString:(NSString *)urlString {
    if (webView) {
        NSURL *url = [[NSURL alloc] initWithString:urlString];

        if (url) {
            [webView loadRequest:[[NSURLRequest alloc] initWithURL:url]];
        }
    }
}

- (void) destroyWebView {
    if (webView) {
        [self hideWebView];

        webView = nil;
    }
}


@end
