#import <Cordova/CDV.h>

@interface INIFrame : CDVPlugin

- (void) create:(CDVInvokedUrlCommand*)command;
- (void) show:(CDVInvokedUrlCommand*)command;
- (void) hide:(CDVInvokedUrlCommand*)command;
- (void) resize:(CDVInvokedUrlCommand*)command;
- (void) navigate:(CDVInvokedUrlCommand*)command;
- (void) destroy:(CDVInvokedUrlCommand*)command;

@end
