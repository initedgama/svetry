#!/bin/sh
VERSION=`grep -e "widget.*version" config.xml | sed "s/.*version=\"\([0-9.]*\)\".*/\1/;s/\./_/g"`
APPNAME=`grep "<name>" config.xml | sed "s/.*<name>\(.*\)<\/name>.*/\1/"`
PROJNAME=svetry

security unlock-keychain -p h
cd platforms/ios
pod install
cd ../..
ionic cordova build ios --device --prod --aot --minifyjs --minifycss --optimizejs --no-interactive
mv platforms/ios/build/device/*.ipa $PROJNAME-$VERSION-$BUILD_NUMBER.ipa
